#ifndef KLAWIATURA_H
#define KLAWIATURA_H
#include <QEvent>
#include <QKeyEvent>
#include <QPaintEvent>
#include "manipulator.h"
//! Obiekt przechwytuje wci�ni�cia klawiszy i na tej podstawie wysy�a sygna�y steruj�ce scen� 3D lub innym obiektem
class Klawiatura : public Manipulator
{
	Q_OBJECT

public:
	//! Konstruktor; klawiatura pocz�tkowo nie jest aktywna
	Klawiatura();
	~Klawiatura();
	//! przechwytuje zdarzenia - je�li wci�ni�to jeden ze zdefiniowanych klawiszy - przetwarza
	bool eventFilter(QObject* object,QEvent* ev);
	//! w kolejnych wersjach - podgl�d obs�uigiwanych klawiszy w nadrz�dnym oknie
	void paintEvent(QPaintEvent *event);
	//! uruchamia dzia�anie manipulatora
	bool start();
	//! zatrzymuje dzia�anie
	void stop();

private:
	//! przechowuje informacj� o tym, czy manipulator powinien dzia�a�
	bool active;
	
};

#endif // KLAWIATURA_H
