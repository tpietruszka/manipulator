#include <QGLViewer/qglviewer.h>
#include<QtOpenGL>
#include<qapplication.h>
#include<QDomElement>
#include<iostream>
#include <lib3ds/file.h>
#include <lib3ds/node.h>
#include <lib3ds/camera.h>
#include <lib3ds/mesh.h>
#include <lib3ds/material.h>
#include <lib3ds/matrix.h>
#include <lib3ds/vector.h>
#include <lib3ds/light.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <qfiledialog.h>
using namespace std;
using namespace qglviewer;

//! Klasa wy�witelaj�ca obiekt wczytany z pliku 3DS, sloty steruj�ce po�o�eniem kamery
class ScenaGL :
	public QGLViewer
{
	Q_OBJECT
public:
	ScenaGL(QWidget * parent);
	~ScenaGL();
	//! Wprowadza ustawienia okna 
	void init();
	//! Rysuje scen� - wywo�ywane przy ka�dym od�wie�eniu okna i zmianie
	void draw();
public slots:
	//! wprowadza obr�t obiektu o zadane k�ty wok� kolejnych osi
	void obroc(float x, float y, float z);
	//! przesuwa kamer� o wektor [x y z]
	void przesun(float x, float y, float z);
	//! pobiera od u�ytkownika nazw� pliku i wczytuje go
	bool wczytajScene();

private:
	//! pozycja kamery 
	float posX, posY, posZ;
	//! k�ty obrotu obiektu
	float rotX, rotY, rotZ;
	//! plik sceny 3DS
	 Lib3dsFile *file;
	//! nazwa kamery z pliku 3DS
  char* camera_name;
  //! wprowadza ustawienia openGL
  void initScene();
  //! wy�wietla w�ze� wczytany z pliku 3DS
  //! Uwaga: funkcja pochodzi z przyk��dowego programu biblioteki libQGLViewer, rozpowszechnianego na zasadach GPL
  void renderNode(Lib3dsNode *node);
  //! Obecna wersja nie obs�uguje animacji - do wprowadzenia w przysz�o�ci
  void animate();
};
