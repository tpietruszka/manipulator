#include "colorPicker.h"

colorPicker::colorPicker(Kamera * camera, QWidget * parent): QLabel(parent), timer(NULL)
{
	cam=camera;
	if(!cam->valid)
		return;
	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(grabFrame()));
	image=cam->queryFrame();
	w = cam->width;
	h = cam->height;
	channels = cam->nChannels;
	smallImage = cvCreateImage(cvSize(640, 480), 8, 3);//obraz do wyswietlenia jako podglad
	qimg = new QImage(640, 480, QImage::Format_ARGB32);
	this->resize(640, 640);
	this->show();
	timer->start(0);
	hsvImage = cvCreateImage(cvSize(w, h), 8, 3);

}

colorPicker::~colorPicker(void)
{
	if(timer)
		delete timer;
}
void colorPicker::grabFrame(){
	image = cam->queryFrame();
	cvResize(image, smallImage);
	ipl2q(smallImage, qimg, w, h, channels);
	setPixmap(QPixmap::fromImage(*qimg));

}
void colorPicker::mousePressEvent(QMouseEvent * e){
	int R, G, B;
	B= ((uchar*)(image->imageData + image->widthStep*e->y()))[e->x()*3];
	G= ((uchar*)(image->imageData + image->widthStep*e->y()))[e->x()*3+1];
	R= ((uchar*)(image->imageData + image->widthStep*e->y()))[e->x()*3+2];
	cvCvtColor(image, hsvImage, CV_BGR2HSV);
	int H, S, V;
	H= ((uchar*)(hsvImage->imageData + hsvImage->widthStep*e->y()))[e->x()*3];
	S= ((uchar*)(hsvImage->imageData + hsvImage->widthStep*e->y()))[e->x()*3+1];
	V= ((uchar*)(hsvImage->imageData + hsvImage->widthStep*e->y()))[e->x()*3+2];
	emit picked(R, G, B);

}
void colorPicker::ipl2q(IplImage *iplImg, QImage * qimg, int w, int h, int channels) //konwersja z iplImage do Qimage
{

	char *data = iplImg->imageData;

	for (int y = 0; y < h; y++, data += iplImg->widthStep)
	{
		for (int x = 0; x < w; x++)
		{
			char r, g, b, a = 0;
			if (channels == 1)
			{
				r = data[x * channels];
				g = data[x * channels];
				b = data[x * channels];
			}
			else if (channels == 3 || channels == 4)
			{
				r = data[x * channels + 2];
				g = data[x * channels + 1];
				b = data[x * channels];
			}

			if (channels == 4)
			{
				a = data[x * channels + 3];
				qimg->setPixel(x, y, qRgba(r, g, b, a));
			}
			else
			{
				qimg->setPixel(x, y, qRgb(r, g, b));
			}
		}
	}


}