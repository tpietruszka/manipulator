#pragma once
#include <QtGui>
#include <QLabel>
#include <QTimer>
#include <QWidget>
#include "highgui.h"
#include "cv.h"
#include "kamera.h"
//! klasa pobieraj�ca obraz z kamery (obiekt Kamera), wy�wietlaj�ca go, po klikni�ciu na dany piksel wysy�aja sygna� z jego sk�adowymi RGB
class colorPicker :
	public QLabel
{Q_OBJECT
public:
	//! wymaga dzia�aj�cej, utworzonej wcze�niej kamery
	colorPicker(Kamera * camera, QWidget * parent);
	~colorPicker(void);
	
	Kamera * cam;
	//! konwersja obrazu z formatu OpenCV (C API) do QImage
	static void ipl2q(IplImage *iplImg, QImage * qimg, int w, int h, int channels);
	public slots:
		//! uaktualnia wy�wietlan� klatk�
		void grabFrame();

	
private:
	//! po naci�ni�ciu przycisku myszy - sk��dowe piksela s� wysy�ane w sygnale
	void mousePressEvent(QMouseEvent * e);

	//! dane obrazu z kamery
	int w, h, channels; 
	QTimer* timer;

	//! ostatni pobrany obraz
	IplImage * image;
	//! obrazy po�rednie
	IplImage * smallImage;
	IplImage * hsvImage;
	QImage* qimg;



signals: void picked (int R, int G, int B);
};
