/********************************************************************************
** Form generated from reading UI file 'Calibrator.ui'
**
** Created: Wed 25. Jul 06:13:48 2012
**      by: Qt User Interface Compiler version 4.7.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CALIBRATOR_H
#define UI_CALIBRATOR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Calibrator
{
public:
    QPushButton *setRedButton;
    QPushButton *setGreenButton;
    QPushButton *setBlueButton;
    QPushButton *saveButton;
    QPushButton *rgbButton;

    void setupUi(QWidget *Calibrator)
    {
        if (Calibrator->objectName().isEmpty())
            Calibrator->setObjectName(QString::fromUtf8("Calibrator"));
        Calibrator->resize(788, 612);
        setRedButton = new QPushButton(Calibrator);
        setRedButton->setObjectName(QString::fromUtf8("setRedButton"));
        setRedButton->setGeometry(QRect(180, 30, 75, 23));
        setGreenButton = new QPushButton(Calibrator);
        setGreenButton->setObjectName(QString::fromUtf8("setGreenButton"));
        setGreenButton->setGeometry(QRect(290, 30, 75, 23));
        setBlueButton = new QPushButton(Calibrator);
        setBlueButton->setObjectName(QString::fromUtf8("setBlueButton"));
        setBlueButton->setGeometry(QRect(400, 30, 75, 23));
        saveButton = new QPushButton(Calibrator);
        saveButton->setObjectName(QString::fromUtf8("saveButton"));
        saveButton->setGeometry(QRect(520, 30, 75, 23));
        rgbButton = new QPushButton(Calibrator);
        rgbButton->setObjectName(QString::fromUtf8("rgbButton"));
        rgbButton->setGeometry(QRect(640, 30, 101, 23));

        retranslateUi(Calibrator);

        QMetaObject::connectSlotsByName(Calibrator);
    } // setupUi

    void retranslateUi(QWidget *Calibrator)
    {
        Calibrator->setWindowTitle(QApplication::translate("Calibrator", "Form", 0, QApplication::UnicodeUTF8));
        setRedButton->setText(QApplication::translate("Calibrator", "Czerwony", 0, QApplication::UnicodeUTF8));
        setGreenButton->setText(QApplication::translate("Calibrator", "Zielony", 0, QApplication::UnicodeUTF8));
        setBlueButton->setText(QApplication::translate("Calibrator", "Niebieski", 0, QApplication::UnicodeUTF8));
        saveButton->setText(QApplication::translate("Calibrator", "Zapisz", 0, QApplication::UnicodeUTF8));
        rgbButton->setText(QApplication::translate("Calibrator", "Prze\305\202\304\205cz RGB-BGR", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Calibrator: public Ui_Calibrator {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CALIBRATOR_H
