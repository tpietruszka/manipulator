/********************************************************************************
** Form generated from reading UI file 'okno.ui'
**
** Created: Wed 25. Jul 20:22:40 2012
**      by: Qt User Interface Compiler version 4.7.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OKNO_H
#define UI_OKNO_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OknoClass
{
public:
    QWidget *centralWidget;
    QPushButton *loadSceneButton;
    QFrame *sceneBox;
    QPushButton *analizatorStart;
    QPushButton *analizatorStop;
    QLabel *label;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *OknoClass)
    {
        if (OknoClass->objectName().isEmpty())
            OknoClass->setObjectName(QString::fromUtf8("OknoClass"));
        OknoClass->resize(1000, 750);
        centralWidget = new QWidget(OknoClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        loadSceneButton = new QPushButton(centralWidget);
        loadSceneButton->setObjectName(QString::fromUtf8("loadSceneButton"));
        loadSceneButton->setGeometry(QRect(200, 650, 81, 23));
        sceneBox = new QFrame(centralWidget);
        sceneBox->setObjectName(QString::fromUtf8("sceneBox"));
        sceneBox->setGeometry(QRect(0, 0, 800, 600));
        sceneBox->setFrameShape(QFrame::StyledPanel);
        sceneBox->setFrameShadow(QFrame::Raised);
        analizatorStart = new QPushButton(centralWidget);
        analizatorStart->setObjectName(QString::fromUtf8("analizatorStart"));
        analizatorStart->setGeometry(QRect(420, 650, 75, 23));
        analizatorStop = new QPushButton(centralWidget);
        analizatorStop->setObjectName(QString::fromUtf8("analizatorStop"));
        analizatorStop->setGeometry(QRect(500, 650, 75, 23));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(430, 630, 61, 16));
        OknoClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(OknoClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        OknoClass->setStatusBar(statusBar);

        retranslateUi(OknoClass);

        QMetaObject::connectSlotsByName(OknoClass);
    } // setupUi

    void retranslateUi(QMainWindow *OknoClass)
    {
        OknoClass->setWindowTitle(QApplication::translate("OknoClass", "Okno", 0, QApplication::UnicodeUTF8));
        loadSceneButton->setText(QApplication::translate("OknoClass", "Wczytaj scen\304\231", 0, QApplication::UnicodeUTF8));
        analizatorStart->setText(QApplication::translate("OknoClass", "Start", 0, QApplication::UnicodeUTF8));
        analizatorStop->setText(QApplication::translate("OknoClass", "Stop", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("OknoClass", "Manipulator:", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class OknoClass: public Ui_OknoClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OKNO_H
