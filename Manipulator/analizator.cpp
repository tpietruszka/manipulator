#include "analizator.h"

using namespace cv;
Analizator::Analizator(QWidget * parent): Manipulator(parent)
{
	timer = new QTimer(this);
	calibrationResults = new QList<CvScalar>;
	kamera = new Kamera();
	if(!kamera->valid)
		return;
	image = kamera->queryFrame();
	connect(timer, SIGNAL(timeout()), this, SLOT(analizuj()));
	timer->setInterval(0);
	




}

Analizator::~Analizator()
{
	stop();

	delete timer;
	delete kamera;
	delete calibrationResults;

}
void Analizator::calibrated()
{
	imageSize = Size(kamera->width, kamera->height);
	hsvImage = Mat(imageSize, 8, 3);
	redThresh1 = Mat(imageSize, 8, 1);
	redThresh2 = Mat(imageSize, 8, 1);
	greenThresh = Mat(imageSize, 8, 1);
	blueThresh = Mat(imageSize, 8, 1);

	Scalar calibMin = calibrationResults->at(0);
	Scalar calibMax = calibrationResults->at(1);


	redMin1 = Scalar(calibMax.val[0]-10, calibMin.val[1]-10, calibMin.val[2]-20);
	redMax1 = Scalar(255, 255, 255);
	redMin2 = Scalar(0, calibMin.val[1]-10, calibMin.val[2]-20);
	redMax2 = Scalar(calibMin.val[0]+10, 255, 255);



	calibMin = calibrationResults->at(2);
	calibMax = calibrationResults->at(3);
	greenMin = Scalar(calibMin.val[0]-10, calibMin.val[1]-10, calibMin.val[2]-20);
	greenMax = Scalar(calibMax.val[0]+10, 255, 255);

	calibMin = calibrationResults->at(4);
	calibMax = calibrationResults->at(5);
	blueMin = Scalar(calibMin.val[0]-10, calibMin.val[1]-10, calibMin.val[2]-20);
	blueMax = Scalar(calibMax.val[0]+10, 255, 255);

	top = 0.25*imageSize.height;
	bottom = 0.75*imageSize.height;

	smallDistance = 0.25*imageSize.height;
	bigDistance = 0.75*imageSize.height;

	Bright = 0.8*imageSize.width;
	Bleft = 0.6*imageSize.width;

	RGright = 0.4 * imageSize.width;
	RGleft = 0.2*imageSize.width;

	timer->start();
}
bool Analizator::start(){
	calibrator = new Calibrator(calibrationResults, kamera, 0);
	connect(calibrator, SIGNAL(calibrated()), this, SLOT(calibrated()));
	return true;
}
void Analizator::stop()
{
	timer->stop();
}
void Analizator::paintEvent(QPaintEvent * event){


}
void Analizator::analizuj()
{
	image = cv::cvarrToMat(kamera->queryFrame());

	cvtColor(image, hsvImage, CV_BGR2HSV);
	medianBlur(hsvImage, hsvImage, 5);

	inRange(hsvImage, redMin1, redMax1, redThresh1);
	inRange(hsvImage, redMin2, redMax2, redThresh2);
	add(redThresh1, redThresh2, redThresh1);
	//imshow("red", redThresh1);

	inRange(hsvImage, greenMin, greenMax, greenThresh);
	//imshow("green", greenThresh);

	inRange(hsvImage, blueMin, blueMax, blueThresh);
	//imshow("blue", blueThresh);

	std::vector<std::vector<Point> > contours; //wyj�ciowa macierz dla findContours

	Mat * thresh;
	Mat * inputs[] = {&redThresh1, &greenThresh, &blueThresh};
	for(int t=0; t<3; t++) //dla ka�dego z kana��w po progowaniu - przetwarzanie jest jednakowe
	{
		thresh = inputs[t];

		//erode(*thresh, *thresh, Mat());
		findContours(*thresh, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
		double maxArea=0;
		int biggestContId=0;
		double currentArea;
		for(int i=0; i < contours.size(); i++)
		{
			currentArea=contourArea(contours[i]);
			if(currentArea > maxArea)
			{
				maxArea = currentArea;
				biggestContId=i;
			}
		}
		if(contours.size() > 0)
		{
			RotatedRect markerBounds = minAreaRect(contours[biggestContId]);
			//kontur najwiekszego obszaru
			drawContours(image, contours, biggestContId, Scalar(255, 0, 0), 4);
			if(maxArea> 500 && (markerBounds.size.area()*0.5 < maxArea))
			{		//znaleziono marker
				markerPositions[t] = markerBounds.center;

				//rysowanie prostokata opisanego
				Point2f rect[4];
				markerBounds.points(rect);
				for(int i = 0; i < 4; i++ )
					line(image, rect[i], rect[(i+1)%4], Scalar(0, 255, 0), 4);
			}else{
				markerPositions[t] = Point2f(-1, -1);
			}
		}else{
				markerPositions[t] = Point2f(-1, -1);
			}


	}
	imshow("conts", image);

	Point R=markerPositions[0], G=markerPositions[1], B=markerPositions[2];
	bool r, g, b;
	//marker dzia�a, jesli jest na w�a�ciwej ~po�owie obrazu
	r = (R.x != -1 && R.x < 0.6*imageSize.width);
	g = (G.x != -1 && G.x < 0.6*imageSize.width);
	b = (B.x != -1) && (B.x > 0.4*imageSize.width);

	//prawa strona obrazu, 
	//wychylenia gora/dol i prawo/lewo powoduja obrot obiektu
	if(b)
	{
		if(B.y < top)
			emit obroc(1, 0,0);
		else if(B.y > bottom)
			emit obroc(-1, 0, 0);
		if(B.x > Bright)
			emit obroc(0, 1, 0);
		else if(B.x < Bleft)
			emit obroc(0, -1, 0);
	}
	//lewa strona obrazu
	//jesli oba markery sa wykrywane: istotna jest odleglosc miedzy nimi
	if(r && g){
		float dist = sqrt((1.*R.x-G.x)*(1.*R.x-G.x)+(1.*R.y-G.y)*(1.*R.y-G.y));
		if(dist > bigDistance)
			emit przesun(0, -1, 0);
		if(dist < smallDistance)
			emit przesun(0, 1, 0);
	}else if (g){
		//jesli jest tylko zielony - wychylenie ze srodka powoduje przesuniecie
		if(G.y < top)
			emit przesun(0, 0, -1);
		else if(G.y > bottom)
			emit przesun(0, 0, 1);
		if(G.x > RGright)
			emit przesun(1, 0, 0);
		else if(G.x < RGleft)
			emit przesun(-1, 0, 0);
	}
}