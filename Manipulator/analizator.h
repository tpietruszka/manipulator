#pragma once
#include "manipulator.h"
#include "kamera.h"
#include "Calibrator.h"

#include<QTGui>
#include<iostream>

using namespace cv;
//! Obiekt przetwarzaj�cy obraz z kamery na sygna�y steruj�ce scen� 3D (lub innym obiektem)
class Analizator :
	public Manipulator
{
	Q_OBJECT
public:
	//!Zaczyna przechwytywa� kamer�, nie uruchamia manipulatora
	Analizator(QWidget * parent=0);
	~Analizator();
	//! W wersjach kolejnych - wy�wietlanie podgl�du w nadrz�dnym oknie
	void paintEvent(QPaintEvent * event);
	//! Ostatni pobrany-aktualnie przetwarzany obraz
	Mat image;
public slots:
	//! analizuje obraz i ewentualnie wysy�a sygna�y
	void analizuj();
	//! po kalibracji - przetwarza pobrane kolory na ustawienia analizatora
	void calibrated();
	//! uruchamia pobieranie analiz� kolejnych klatek
	bool start();
	void stop();
private:
	//! timer uruchamiaj�cy analiz� kolejnych zdj��
	QTimer * timer;
	Kamera * kamera;
	Calibrator * calibrator;
	//! Po zako�czeniu dzia�ania kalibratora zawieraj� wyniki jego dzia�ania
	QList<CvScalar> * calibrationResults;
	
	//! obraz po zmianie przestrzeni kolor�w
	Mat hsvImage;
	//! obrazy progowane poszczeg�lnych kolor�w (czerwony to dwa zakresy - obrazy s� sumowane do redThresh1)
	Mat redThresh1, redThresh2, greenThresh, blueThresh;
	Size imageSize;

	//! warto�ci skrajne u�ywane do progowania
	Scalar redMin1, redMin2, redMax1, redMax2, greenMin, greenMax, blueMin, blueMax; 
	//! pozycje wykrytych znacznik�w - (-1, -1), je�li nie wykryto
	Point2f markerPositions[3];

	//! granice dzia�ania marker�w
	int top, bottom; 
	int RGleft, RGright, Bleft, Bright;
	//! graniczne odleg�o�ci mi�dzy markerami RG
	int smallDistance, bigDistance; 
	

};
