#include "klawiatura.h"

Klawiatura::Klawiatura()
: Manipulator()
{
	active = false;

}

Klawiatura::~Klawiatura()
{

}
bool Klawiatura::start()
{
	return active=true;
}
void Klawiatura::stop()
{
	active = false;
}
void Klawiatura::paintEvent(QPaintEvent *event){}

bool Klawiatura::eventFilter(QObject* object,QEvent* ev)
{
	if (ev->type() == QEvent::KeyPress && active) 
	{
		QKeyEvent * e = (QKeyEvent*) ev;

		float ile=1;
		if(e->key() == Qt::Key_W)
			emit przesun(0, ile, 0);
		if(e->key() == Qt::Key_S)
			emit przesun(0, -ile, 0);
		if(e->key() == Qt::Key_A)
			emit przesun(-ile, 0, 0);
		if(e->key() == Qt::Key_D)
			emit przesun(ile, 0, 0);
		if(e->key() == Qt::Key_R)
			emit przesun(0, 0, ile);
		if(e->key() == Qt::Key_F)
			emit przesun(0, 0, -ile);
		if(e->key() == Qt::Key_I)
			emit obroc(ile, 0, 0);
		if(e->key() == Qt::Key_K)
			emit obroc(-ile, 0, 0);
		if(e->key() == Qt::Key_J)
			emit obroc(0, ile, 0);
		if(e->key() == Qt::Key_L)
			emit obroc(0, -ile, 0);
		if(e->key() == Qt::Key_P)
			emit obroc(0, 0, ile);
		if(e->key() == Qt::Key_Semicolon)
			emit obroc(0, 0, -ile);
		e->accept();
		return true;
	}
	else 
	{
		return QObject::eventFilter(object, ev);
	}



};