#pragma once

#include <QtGui/QMainWindow>
#include <QImage>
#include <QWidget>
#include <QLabel>
#include <QPixmap>
#include <QTimer>
#include <QMessageBox>
#include <QString>
#include <QFileDialog>
#include "ui_Calibrator.h"
#include "highgui.h"
#include "cv.h"
#include "cxcore.h"
#include <cmath>
#include <vector>
#include "kamera.h"
#include "colorPicker.h"
//! Widget umo�liwiaj�cy u�ytkownikowi wskazanie na obrazie marker�w
/*!
Zak�ada si�,�e s� to trzy markery o r�nych kolorach, optymalnie: czerwony, zielony i niebieslki.
Nale�y wskaza� co najmniej trzy punkty na ka�dym z nich
*/
class Calibrator : public QWidget
{
	Q_OBJECT
public:
	//! Tworzy okno i rozpoczyna dzia�anie kalibratora
	/*!
	\param result lista wype�niana skrajnymi warto�ciami kolor�w
	\param camera wcze�niej przechwycony obiekt kamery
	*/
	Calibrator(QList<CvScalar>* result, Kamera *camera, QWidget* parent=0);
	~Calibrator(void);
public slots:
	//! dodaje wybrany kolor do listy - wybranej przez setCurrentColor()
	void saveColor(int R, int G, int B);
	//! wybiera aktualnie kalibrowany kolor
	/*!
	1-r
	2-g
	3-b
	*/
	void setCurrentColor(int currentColor);
	//! Ko�czy prac� kalibratora i zapisuje wyniki - o ile zebrano dostateczn� ilo�c danych
	void save();
signals:
	//! sygna� emitowany gdy zako�czono prac�
	void calibrated();
private:
	//! usuni�cie obiektu przy zamkni�ciu okna
	void closeEvent(QCloseEvent * event);
	//! znajduje warto�ci skrajne na podstawie listy kolor�w pikseli
	QList<CvScalar> findMinMax(QList<CvScalar> points, int color);

	//! obiekt GUI
	Ui::Calibrator ui;
	//! listy zawieraj�ce sk�adowe RGB punkt�w wewn�trz poszczeg�lnych marker�w
	QList<CvScalar> redPoints, greenPoints, bluePoints;
	//! aktualnie wybierany kolor, poczatkowo NULL
	int currentColor;
	QSignalMapper* signalMapper;
	//! wska�nik do listy wynikowych warto�ci - wype�nianej skrajnymi wybranymi warto�ciami HSV
	QList<CvScalar>* result;
	Kamera *cam;
	colorPicker * picker;

	


};
