#include "scenaGL.h"

using namespace std;
using namespace qglviewer;
ScenaGL::ScenaGL(QWidget * parent): QGLViewer(parent)
{
	std::cout << "Konstruktor sceny\n";

	posX=posY=0;
	posZ=0;
	rotX=rotY=rotZ=0;
	file=NULL;
	camera_name = NULL;
	this->resize(800, 600);

}

ScenaGL::~ScenaGL(void)
{

}
bool ScenaGL::wczytajScene()
{

	QString name = QFileDialog::getOpenFileName(this, "Select a 3ds model", ".", "3DS files (*.3ds *.3DS);;All files (*)");
	if (name.isEmpty())
		return false;

	file = lib3ds_file_load(name.toLatin1());
	if (!file)
	{
		qWarning("Error : Unable to open file ");
		exit(1);
	}

	if (file->cameras)
		camera_name = file->cameras->name;
	else
		camera_name = NULL;

	lib3ds_file_eval(file,0);

	initScene();
	return true;
}
void ScenaGL::init()
{
	glShadeModel(GL_SMOOTH);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glDisable(GL_LIGHT1);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_COLOR_MATERIAL);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	restoreStateFromFile();
	window()->move(0,0);
	window()->resize(1024, 700);
}

void ScenaGL::draw()
{
	if (!file)
		return;
	camera()->setZClippingCoefficient(5);
	showEntireScene();
	

	glTranslatef(-posX, -posY, -posZ);
	glRotatef(rotX, 1, 0, 0);
	glRotatef(rotY, 0, 1, 0);
	glRotatef(rotZ, 0, 0, 1);

	/*Vec camPos = camera()->position();
	Vec viewDir = camera()->viewDirection();
	Vec sCent = camera()->sceneCenter();
	glLoadIdentity();
	gluLookAt(camPos[0]+posX, camPos[1]+posY, camPos[2]+posZ, sCent[0], sCent[1], sCent[2], 1, 0, 0);
	//camera()->frame()->rotateAroundPoint(Quaternion(rotX, 1, 0, 0), Vec(0, 0, 0));*/


	for (Lib3dsNode* p=file->nodes; p!=0; p=p->next)
		renderNode(p);
}

void ScenaGL::obroc(float x, float y, float z)
{	
	rotX += x;
	rotY += y;
	rotZ += z;
	updateGL();

}
void ScenaGL::przesun(float x, float y, float z)
{
	posX += x*0.2;
	posY += y*0.2;
	posZ += z*0.2;
	updateGL();
}
void ScenaGL::initScene()
{
	if (!file)
		return;

	// Lights
	GLfloat amb[] = {0.0, 0.0, 0.0, 1.0};
	GLfloat dif[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat spe[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat pos[] = {0.0, 0.0, 0.0, 1.0};
	int li=GL_LIGHT0;
	for (Lib3dsLight* l=file->lights; l; l=l->next)
	{
		glEnable(li);

		glLightfv(li, GL_AMBIENT,  amb);
		glLightfv(li, GL_DIFFUSE,  dif);
		glLightfv(li, GL_SPECULAR, spe);

		pos[0] = l->position[0];
		pos[1] = l->position[1];
		pos[2] = l->position[2];
		glLightfv(li, GL_POSITION, pos);

		if (!l->spot_light)
			continue;

		pos[0] = l->spot[0] - l->position[0];
		pos[1] = l->spot[1] - l->position[1];
		pos[2] = l->spot[2] - l->position[2];
		glLightfv(li, GL_SPOT_DIRECTION, pos);
		++li;
	}
}
void ScenaGL::renderNode(Lib3dsNode *node)
{
	for (Lib3dsNode* p=node->childs; p!=0; p=p->next)
		renderNode(p);

	if (node->type == LIB3DS_OBJECT_NODE)
	{
		if (strcmp(node->name,"$$$DUMMY")==0)
			return;

		if (!node->user.d)
		{
			Lib3dsMesh *mesh=lib3ds_file_mesh_by_name(file, node->name);
			if (!mesh)
				return;

			node->user.d = glGenLists(1);
			glNewList(node->user.d, GL_COMPILE);

			Lib3dsVector *normalL = new Lib3dsVector[3*mesh->faces];

			Lib3dsMatrix M;
			lib3ds_matrix_copy(M, mesh->matrix);
			lib3ds_matrix_inv(M);
			glMultMatrixf(&M[0][0]);

			lib3ds_mesh_calculate_normals(mesh, normalL);

			for (unsigned int p=0; p<mesh->faces; ++p)
			{
				Lib3dsFace *f=&mesh->faceL[p];
				Lib3dsMaterial *mat=0;
				if (f->material[0])
					mat=lib3ds_file_material_by_name(file, f->material);

				if (mat)
				{
					static GLfloat a[4]={0,0,0,1};
					float s;
					glMaterialfv(GL_FRONT, GL_AMBIENT, a);
					glMaterialfv(GL_FRONT, GL_DIFFUSE, mat->diffuse);
					glMaterialfv(GL_FRONT, GL_SPECULAR, mat->specular);
					s = pow(2, 10.0*mat->shininess);
					if (s>128.0)
						s=128.0;
					glMaterialf(GL_FRONT, GL_SHININESS, s);
				}
				else
				{
					Lib3dsRgba a={0.2, 0.2, 0.2, 1.0};
					Lib3dsRgba d={0.8, 0.8, 0.8, 1.0};
					Lib3dsRgba s={0.0, 0.0, 0.0, 1.0};
					glMaterialfv(GL_FRONT, GL_AMBIENT, a);
					glMaterialfv(GL_FRONT, GL_DIFFUSE, d);
					glMaterialfv(GL_FRONT, GL_SPECULAR, s);
				}

				glBegin(GL_TRIANGLES);
				glNormal3fv(f->normal);
				for (int i=0; i<3; ++i)
				{
					glNormal3fv(normalL[3*p+i]);
					glVertex3fv(mesh->pointL[f->points[i]].pos);
				}
				glEnd();
			}

			delete[] normalL;

			glEndList();
		}

		if (node->user.d)
		{
			glPushMatrix();
			Lib3dsObjectData* d = &node->data.object;
			glMultMatrixf(&node->matrix[0][0]);
			glTranslatef(-d->pivot[0], -d->pivot[1], -d->pivot[2]);
			glCallList(node->user.d);
			glPopMatrix();
		}
	}
}

void ScenaGL::animate()
{
}


