#include "Calibrator.h"

Calibrator::Calibrator(QList<CvScalar>* result, Kamera *camera, QWidget *parent): QWidget(parent)
{
	ui.setupUi(this);
	cam=camera;
	picker = new colorPicker(cam, this);
	picker->setGeometry(200, 200, 640, 480);
	picker->show();
	currentColor = NULL;

	this->result = result;

	connect(picker, SIGNAL(picked(int, int, int)), this, SLOT(saveColor(int, int, int)));

	signalMapper = new QSignalMapper(this);
    signalMapper->setMapping(ui.setRedButton, 1);
	signalMapper->setMapping(ui.setGreenButton, 2);
	signalMapper->setMapping(ui.setBlueButton, 3);

	connect(ui.setRedButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
	connect(ui.setGreenButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
	connect(ui.setBlueButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(setCurrentColor(int)));
	connect(ui.rgbButton, SIGNAL(clicked()), cam, SLOT(switchRgb()));

	connect(ui.saveButton, SIGNAL(clicked()), this, SLOT(save()));
	setWindowTitle("Kalibracja");
	show();

}
Calibrator::~Calibrator(){
	if(picker)
		delete picker;
}
void Calibrator::closeEvent(QCloseEvent * event)
{
	delete this;
}
void Calibrator::save(){

	if(redPoints.count() < 3 || greenPoints.count() < 3 || bluePoints.count()<3)
	{
		QMessageBox::warning(0, "Blad", "Za malo wybranych punktow");
		return;
	}
	if(result->count()>0) 
		result->clear();
	*result << findMinMax(redPoints, 1);
	*result << findMinMax(greenPoints, 2);
	*result << findMinMax(bluePoints, 3);
	emit calibrated();
	close();
}

// 1-red, 2-green, 3-blue
QList<CvScalar> Calibrator::findMinMax(QList<CvScalar> points, int color){
	QList<CvScalar> minMax;//zmienna do zwrotu
	if(points.count() == 0)
		return minMax; //brak punktow
	int minH, minS, minV, maxH, maxS, maxV;
	if(color == 1){//zalozenie, ze czerwony po obu stronach bieguna
		minH=0;
		maxH=255;
	}else{
		minH=points[0].val[0];
		maxH=points[0].val[0];
	}
	minS=points[0].val[1];
	minV=points[0].val[2];
	maxS=points[0].val[1];
	maxV=points[0].val[2];

	for(int i=0;i<points.count();i++)
	{
		if(color==1)
		{   
			if(points[i].val[0] > 120 && points[i].val[0] < maxH) maxH=points[i].val[0];
			if(points[i].val[0] < 120 && points[i].val[0] > minH) minH=points[i].val[0];
		}else{
			if(points[i].val[0] < minH) minH=points[i].val[0];
			if(points[i].val[0] > maxH) maxH=points[i].val[0];
		}
		if(points[i].val[1] < minS) minS=points[i].val[1];
		if(points[i].val[1] > maxS) maxS=points[i].val[1];
		if(points[i].val[2] < minV) minV=points[i].val[2];
		if(points[i].val[2] > maxV) maxV=points[i].val[2];

	}
	
	minMax << cvScalar(minH, minS, minV); //wszystko na MIN
	minMax <<cvScalar(maxH, maxS, maxV);
	return minMax;

}

void Calibrator::saveColor(int R, int G, int B)
{
	if(!currentColor)
		return;
	float red, green, blue;
	red = R/255.0;
	green = G/255.0;
	blue = B/255.0;

	float hue, sat, val;
	float x, f, i;
 
	x = MIN(MIN(red, green), blue);
	val = MAX(MAX(red, green), blue);
	if (x == val){
		hue = 0;
	sat = 0;
	}
	else {
		f = (red == x) ? green-blue : ((green == x) ? blue-red : red-green);
		i = (red == x) ? 3 : ((green == x) ? 5 : 1);
		hue = fmod((i-f/(val-x))*60, 360);
		sat = ((val-x)/val);
	}
	int H, S, V;
	H=hue*180/360;
	S=sat*255;
	V=val*255;
	CvScalar punkt =  cvScalar(H, S, V);
	if(currentColor == 1)
		redPoints.push_back(punkt);
	else if(currentColor == 2)
		greenPoints.push_back(punkt);
	else if(currentColor == 3)
		bluePoints.push_back(punkt);


}
void Calibrator::setCurrentColor(int currentColor)
{
	this->currentColor = currentColor;
}
//
//Calibrator::Calibrator(CvMat * dstIntrinsic, CvMat * dstDistortion, QWidget* parent) : QWidget(parent)
//{
//	returnIntrinsic = dstIntrinsic;
//	returnDistortion = dstDistortion;
//
//	ui.setupUi(this);
//	setWindowTitle("Kalibracja");
//	connect(ui.startButton, SIGNAL(clicked()), this, SLOT(start()));
//	connect(ui.resetButton, SIGNAL(clicked()), this, SLOT(reset()));
//	connect(ui.saveToFileButton, SIGNAL(clicked()), this, SLOT(saveToFile()));
//	connect(ui.saveSettingsButton, SIGNAL(clicked()), this, SLOT(saveSettings()));
//	connect(ui.addPhotosButton, SIGNAL(clicked()), this, SLOT(loadPhotos()));
//
//	calibrated=false;
//	photo = ui.previewLabel;
//	frames=0;
//	image=NULL;
//	smallImage = cvCreateImage(cvSize(640, 480), 8, 3);//obraz do wyswietlenia jako podglad
//	qimg = new QImage(640, 480, QImage::Format_ARGB32);
//
//	intrinsic_matrix = cvCreateMat(3,3,CV_32FC1);
//	distortion_coeffs = cvCreateMat(5,1,CV_32FC1);
//	
//	timerPrev = new QTimer(this);
//	connect(timerPrev, SIGNAL(timeout()), this, SLOT(download()));
//	
//	capture=NULL;
//
//
//
//	this->show();
//}
//
//Calibrator::~Calibrator(void)
//{
//	if(capture)
//		cvReleaseCapture(&capture);
//}
//
//void Calibrator::calibrate(){
//	CV_MAT_ELEM( *intrinsic_matrix, float, 0, 0 ) = 1.0f;
//	CV_MAT_ELEM( *intrinsic_matrix, float, 1, 1 ) = 1.0f;	
//	cvCalibrateCamera2(objectCords, pixelCords, cornersInEach, boardSize, intrinsic_matrix, distortion_coeffs, NULL, NULL, 0);
//	
//	calibrated=true;
//	ui.messages->setText("Kalibracja poprawna");
//	ui.saveToFileButton->setEnabled(1);
//	ui.saveSettingsButton->setEnabled(1);
//
//
//
//	
//}
//void Calibrator::saveToFile(){
//	if(!calibrated){
//		QMessageBox::warning(this,  "B��d!", "System nie jest skalibrowany, nie mo�na zapisa�");
//		return;
//	}
//	QString fileName = QFileDialog::getSaveFileName(this, "Zapisz plik", "", ".xml");
//	CvFileStorage * file = cvOpenFileStorage(fileName.toLatin1(), 0, CV_STORAGE_WRITE);
//	cvWrite(file, "intrinsic_matrix", intrinsic_matrix);
//	cvWrite(file, "distortion_coeffs", distortion_coeffs);
//	cvReleaseFileStorage(&file);
//	
//
//
//	
//}
//
//void Calibrator::analyse(){
//	if(!prepared)
//		prepare();
//
//	int cornerCount, found;
//	found = cvFindChessboardCorners(image, boardSize, tempCorners, &cornerCount, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
//	cvCvtColor(image, grayImage, CV_BGR2GRAY);
//	cvFindCornerSubPix(grayImage, tempCorners, cornerCount,
//		cvSize(11,11),cvSize(-1,-1), cvTermCriteria( 
//		CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));
//
//	if(cornerCount == cornersOnBoard) //jesli znaleziono wlasciwa ilosc wierzcholkow, dodanie danych o klatce
//	{
//		for(int i=frames*cornersOnBoard, j=0; j<cornersOnBoard; ++i, ++j)
//		{
//			CV_MAT_ELEM(*pixelCords, float, i, 0)=tempCorners[j].x;
//			CV_MAT_ELEM(*pixelCords, float, i, 1)=tempCorners[j].y;
//			CV_MAT_ELEM(*objectCords, float, i, 0)=j/boardSize.width;		
//			CV_MAT_ELEM(*objectCords, float, i, 1)=j%boardSize.width;		
//			CV_MAT_ELEM(*objectCords, float, i, 2)=0;	
//		}
//		CV_MAT_ELEM(*cornersInEach, int,frames,0) = cornersOnBoard;
//		frames++;
//
//
//
//	}
//	if(frames >= minFrames){
//		timerPrev->stop();
//		ui.addPhotosButton->setEnabled(0);
//		calibrate();
//		return;
//	}
//	this->repaint();
//
//}
//void Calibrator::prepare(){
//	
//	h = image->height;
//	w = image->width;
//	channels = image->nChannels;
//	grayImage = cvCreateImage(cvGetSize(image),8,1);//do analizy subpixelowej
//
//	int boardWidth=ui.boardWidth->value();
//	int boardHeight=ui.boardHeight->value();
//	int minFramesQuantity=ui.minFrames->value();
//	
//
//	boardSize = cvSize(boardWidth, boardHeight);
//	cornersOnBoard = boardWidth * boardHeight;
//	minFrames = minFramesQuantity;
//
//	pixelCords = cvCreateMat(minFrames*cornersOnBoard, 2, CV_32FC1);
//	objectCords = cvCreateMat(minFrames*cornersOnBoard, 3, CV_32FC1);
//	cornersInEach = cvCreateMat(minFrames, 1, CV_32SC1);
//
//	tempCorners = new CvPoint2D32f[cornersOnBoard];
//}
//void Calibrator::start(){
//	this->capture = cvCreateCameraCapture( 0 );
//	if(!this->capture){
//		QMessageBox::warning(this,  "B��d!", "Nie wykryto kamery");
//		this->capture = 0;
//		return;
//	}
//
//	ui.startButton->setEnabled(0);
//	ui.boardHeight->setEnabled(0);
//	ui.boardWidth->setEnabled(0);
//	ui.minFrames->setEnabled(0);
//
//
//	image = cvQueryFrame(capture); 
//	prepare();
//	timerPrev->start(0);
//	connect(this, SIGNAL(grabFrame()), this, SLOT(analyse()));
//	ui.messages->setText("Kliknij w dowolnym miejscu, aby zapisac klatke");
//
//}
//void Calibrator::reset(){
//	disconnect(this, SIGNAL(grabFrame()), this, SLOT(analyse()));
//	if(capture)
//	{
//		cvReleaseCapture(&capture);
//		capture=NULL;
//	}
//
//	ui.startButton->setEnabled(1);
//	ui.boardHeight->setEnabled(1);
//	ui.boardWidth->setEnabled(1);
//	ui.minFrames->setEnabled(1);
//	frames=0;
//	calibrated=0;
//	prepared=0;
//	image=NULL;
//	ui.saveToFileButton->setEnabled(0);	
//	ui.saveSettingsButton->setEnabled(0);
//	timerPrev->stop();
//
//	ui.messages->setText("");
//
//}
//void Calibrator::mousePressEvent(QMouseEvent *event)
//{
//	emit grabFrame();
//}
//
//void Calibrator::paintEvent ( QPaintEvent * event ){
//
//	if(image){
//		cvResize(image, smallImage);
//		Calibrator::ipl2q(smallImage, qimg, w, h, channels);
//		photo->setPixmap(QPixmap::fromImage(*qimg));
//	}
//	ui.frameCounter->setText(QString::number(frames));
//
//}
//void Calibrator::download(){
//	image = cvQueryFrame(capture);
//	this->update();
//}
//void Calibrator::saveSettings()
//{
//	
//	cvCopy(intrinsic_matrix, returnIntrinsic);
//	cvCopy(distortion_coeffs, returnDistortion);
//
//}
//void Calibrator::closeEvent(QCloseEvent * event)
//{
//	delete this;
//}
//void Calibrator::loadPhotos(){
//	ui.startButton->setEnabled(0);
//	ui.addPhotosButton->setText("Dodaj zdjecia");
//	
//	QStringList fileNames = QFileDialog::getOpenFileNames(this, tr("Open Image"), "", tr("Image Files (*.png *.jpg *.bmp *.avi *.gif)"));
//	if(fileNames.isEmpty()){
//		return;
//	}
//	image = cvLoadImage(fileNames.at(0).toLatin1());	
//	prepare();//po wczytaniu pierwszego zdjecia
//
//	foreach(QString name, fileNames){
//		image = cvLoadImage(name.toLatin1());
//		analyse();
//	}
//	ui.messages->setText(QString("Dodaj kolejne zdjecia, pozostalo: ").append(QString::number(minFrames-frames))); //po"prepare" i przetwarzaniu
//
//
//}
//bool Calibrator::loadFromFile(CvMat * dstIntrinsic, CvMat * dstDistortion, QString fileName)
//{
//	CvMat * intr = cvCreateMat(3,3,CV_32FC1);
//	CvMat * dstr = cvCreateMat(5,1,CV_32FC1);
//	QString fileToRead;
//	if(fileName==NULL || fileName.isEmpty())
//	{
//		fileToRead = QFileDialog::getOpenFileName(0, "Wybierz plik do wczytania", "", "");
//		if(fileToRead.isEmpty())
//		{
//			QMessageBox::warning(0, "Blad", "Nie wybrano pliku z danymi");
//			return 0;
//		}
//	}else{
//		fileToRead = fileName;
//	}
//	CvFileStorage * file = cvOpenFileStorage(fileToRead.toLatin1(), 0, CV_STORAGE_READ);
//	intr = (CvMat*) cvReadByName(file, 0, "intrinsic_matrix");
//	dstr = (CvMat*) cvReadByName(file, 0, "distortion_coeffs");
//	cvReleaseFileStorage(&file);
//
//	cvCopy(intr, dstIntrinsic);
//	cvCopy(dstr, dstDistortion);
//	return 1;
//
//
//}
//void Calibrator::ipl2q(IplImage *iplImg, QImage * qimg, int w, int h, int channels) //konwersja z iplImage do Qimage
//{
//
//	char *data = iplImg->imageData;
//
//	for (int y = 0; y < h; y++, data += iplImg->widthStep)
//	{
//		for (int x = 0; x < w; x++)
//		{
//			char r, g, b, a = 0;
//			if (channels == 1)
//			{
//				r = data[x * channels];
//				g = data[x * channels];
//				b = data[x * channels];
//			}
//			else if (channels == 3 || channels == 4)
//			{
//				r = data[x * channels + 2];
//				g = data[x * channels + 1];
//				b = data[x * channels];
//			}
//
//			if (channels == 4)
//			{
//				a = data[x * channels + 3];
//				qimg->setPixel(x, y, qRgba(r, g, b, a));
//			}
//			else
//			{
//				qimg->setPixel(x, y, qRgb(r, g, b));
//			}
//		}
//	}
//
//
//}