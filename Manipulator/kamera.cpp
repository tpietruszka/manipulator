#include "kamera.h"

Kamera::Kamera()
{
	

	capture = NULL; 
	width=height=nChannels=0;
	for(int i=10; i>= 0; i--)
	{
		capture = cvCaptureFromCAM(i);
		if (capture) {break;}
	}
	if(!capture)
	{
		QMessageBox::warning(0, "Blad", "Brak dostepnej kamery lub kamera w uzyciu");
		valid=false;
		return;
	}
	valid=true;
	image = cvQueryFrame(capture);
	width = image->width;
	height = image->height;
	nChannels = image->nChannels;
	rgb=false;
}

Kamera::~Kamera()
{
	if(capture)
		cvReleaseCapture(&capture);
}
void Kamera::switchRgb()
{
	rgb = rgb? false: true;
}
IplImage *  Kamera::queryFrame(){

	image = cvQueryFrame(capture);

	if(rgb)//jesli kamera zwraca nieprawidlowa kolejnosc kanalow..
	{
		IplImage * red, *green, *blue;
		red = cvCreateImage(cvGetSize(image),IPL_DEPTH_8U,1);
		green = cvCreateImage(cvGetSize(image),IPL_DEPTH_8U,1);
		blue = cvCreateImage(cvGetSize(image),IPL_DEPTH_8U,1);
		cvCvtPixToPlane(image, red, green, blue, NULL);
		cvCvtPlaneToPix(blue, green, red, NULL, image);
		cvReleaseImage(&red);
		cvReleaseImage(&green);
		cvReleaseImage(&blue);


	}

	return image;

	/*cvCopy(result, temp1);
	image = cvQueryFrame(capture);
	cvAddWeighted(temp1, 0.5, image, 0.5, 0, result);


	return result;*/




	//obraz = cvQueryFrame(camera);
	//if (obraz == NULL)
	//	return 0;
	//
	//do_analizy = cvCreateImage(cvGetSize(obraz), 8, 3);
	//cvimage = cvCreateImage(cvGetSize(obraz), 8, 1);
	//cvCvtColor(obraz, do_analizy, CV_BGR2HSV);
	//cvInRangeS(do_analizy, cvScalar(20, 40, 40), cvScalar(50, 255, 255), cvimage);
	//cvShowImage("normal", obraz);
	//cvShowImage("thresh", cvimage);

	/*ch0=cvCreateImage(cvGetSize(obraz),IPL_DEPTH_8U,1);
	ch1=cvCreateImage(cvGetSize(obraz),IPL_DEPTH_8U,1);
	ch2=cvCreateImage(cvGetSize(obraz),IPL_DEPTH_8U,1);

	do_analizy = cvCreateImage(cvGetSize(obraz),8,3);

	cvCvtPixToPlane(obraz,ch0,ch1,ch2,NULL);
	cvCvtPlaneToPix(ch2,ch2,ch2,NULL,do_analizy);

	// przystowowywanie obrazu

	//cvCvtColor(obraz, do_analizy, CV_BGR2GRAY);

	// nasz kontur
	CvSeq * kontur;
	// pamiec na obliczenia
	CvMemStorage * mem = cvCreateMemStorage(0);
	// operacja progrowania
	//cvThreshold(do_analizy, do_analizy, 100, 255, CV_THRESH_BINARY);
	//cvAdaptiveThreshold(do_analizy, do_analizy, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 80, 5);

	cvShowImage("szarosc", do_analizy);
	/*
	double mean = cvMean(obraz);
	double low_thresh = 0.66 * mean;
	double high_thresh = 1.33 * mean;
	IplImage* cannyImg;
	cannyImg = cvCreateImage(cvGetSize(do_analizy), IPL_DEPTH_8U, 1);
	cvCanny(do_analizy, cannyImg,low_thresh,high_thresh);


	// szukanie konturow
	cvNamedWindow("po progowaniu", CV_WINDOW_AUTOSIZE);
	cvShowImage("po progowaniu", cannyImg);
	cvFindContours(do_analizy, mem, &kontur, sizeof (CvContour));
	//cvCanny(do_analizy, obraz, 100, 150, 3);

	/*for (; kontur != NULL; kontur = kontur->h_next)
	{
	// aproksymacja konturu
	CvSeq* temp_kontur = cvApproxPoly(kontur, sizeof (CvContour), mem, CV_POLY_APPROX_DP, cvContourPerimeter(kontur) * 0.0070);
	// zaznaczanie konturow na obrazie
	cvDrawContours(obraz, temp_kontur, cvScalar(255.0, 0.0, 0.0, 0.0), cvScalar(0.0, 255.0, 0.0, 0.0), 100, 2, CV_AA, cvPoint(0, 0));
	}
	cvNamedWindow("kontury", CV_WINDOW_AUTOSIZE);
	cvShowImage("kontury", obraz);
	*/

	//image = QImage((const uchar *)cvimage->imageData, cvimage->width, cvimage->height, QImage::Format_RGB888);

	//ui.label->setPixmap(QPixmap::fromImage(image.rgbSwapped()));
	//return cvimage;
}
