#include "okno.h"

Okno::Okno(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);
	analizator=NULL;
	klawiatura=NULL;
	scena=NULL;

	
	//analizator = new Analizator(this);
	scena = new ScenaGL(ui.sceneBox);
	klawiatura = new Klawiatura();
	klawiatura->start();
	installEventFilter(klawiatura);
	scena->setFocusProxy(klawiatura);
	klawiatura->setFocusPolicy(Qt::StrongFocus);
	connect(ui.loadSceneButton, SIGNAL(clicked()), scena, SLOT(wczytajScene()));
	connect(ui.analizatorStart, SIGNAL(clicked()), this, SLOT(analizatorStart()));
	connect(klawiatura, SIGNAL(przesun(float, float, float)), scena, SLOT(przesun(float, float, float)));
	connect(klawiatura, SIGNAL(obroc(float, float, float)), scena, SLOT(obroc(float, float, float)));
}

Okno::~Okno()
{
	if(scena)
		delete scena;
	if(analizator)
		delete analizator;
	if(klawiatura)
		delete klawiatura;

}

void Okno::analizatorStart()
{
	if(!analizator)
	{
		analizator = new Analizator(this);
		connect(analizator, SIGNAL(przesun(float, float, float)), scena, SLOT(przesun(float, float, float)));
		connect(analizator, SIGNAL(obroc(float, float, float)), scena, SLOT(obroc(float, float, float)));
		connect(ui.analizatorStop, SIGNAL(clicked()), analizator, SLOT(stop()));
	}
	analizator->start();
}
