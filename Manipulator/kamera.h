#pragma once
#include<iostream>
#include<QImage>
#include<QMessageBox>
#include<cv.h>
#include"highgui.h"
//! Klasa uruchamiaj�ca pobieranie obraz�w z kamery, w razie potrzeby przetwarza obraz przed przekazaniem
class Kamera: public QObject
{
	Q_OBJECT
public:
	//!Konstruktor, tworzy po��czenia
	Kamera();
	~Kamera();
	//! Pobiera now� klatk�, zwraca wska�nik do niej w razie sukcesu 
	IplImage * queryFrame(); 
	//! Dane obrazu z kamery
	int width, height, nChannels;
	//! Status po��czenia z kamer�
	bool valid;
	public slots:
	//! zamienia kana�y R i B w wyj�ciowym obrazie
	void switchRgb();
private:
	//! ostatnio pobrana klatka
	IplImage * image;
	//! "uchwyt" kamery
	CvCapture* capture; 
	//! nale�y ustawi� true, je�li kamera zwraca rgb zamiastr bgr
	bool rgb;
};
