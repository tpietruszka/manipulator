#pragma once
#include<QWidget>
//! Interfejs manipulator�w do sterowania scen�
class Manipulator :
	public QWidget
{
	Q_OBJECT
public:
	Manipulator(QWidget * parent=0): QWidget(parent){};
	virtual ~Manipulator(){};
	//uruchamia dzia�anie manipulatora, domy�lnie jest on wy��czony
	virtual bool start()=0;
	//zatrzymuje dzia�anie
	virtual void stop()=0;
	//opcjonalne-wy�wietlanie widgetu, nie jest konieczne.
	virtual void paintEvent(QPaintEvent * event)=0;
	
signals: 
	//! sygna� do przesuni�cia kamery o zadan� wielko��
	void przesun(float x, float y, float z);
	//! sygna� do obrotu obiektu o zadan� wielko��
	 void obroc(float x, float y, float z);
};
