#ifndef OKNO_H
#define OKNO_H

#include <QtGui>
#include<QImage>
#include "ui_okno.h"
#include "kamera.h"
#include "scenaGL.h"
#include "analizator.h"
#include "klawiatura.h"

//! Klasa tworz�ca g��wne okno, odpowiada za podstawowy interfejs i komunikacj� mi�dzy manipulatorami a scen�
class Okno : public QMainWindow
{
	Q_OBJECT

public:
	//! Tworzy scen� openGL, widget przechwytuj�cy zdarzenia z klawiatury, ��czy sygna�y klawiatura-scena
	Okno(QWidget *parent = 0, Qt::WFlags flags = 0);
	//! Destruktor wywo�ywany przy zamykaniu okna
	~Okno();

	public slots:
	//! Tworzy "analizator" - widget steruj�cy scen� na podst. danych z kamery - ��czy sygna�y ze slotami
	void analizatorStart();
private:
	//! GUI okna
	Ui::OknoClass ui;

	//! analizator steruj�cy scen� - steruje scen� po start()
	Manipulator * analizator;
	//! obiekt przechwytujk�cy wci�ni�cia klawiszy - steruje scen�
	Manipulator * klawiatura;
	//! scena openGL w kt�rej wy�wietlany jest model
	ScenaGL * scena;
};

#endif // OKNO_H
